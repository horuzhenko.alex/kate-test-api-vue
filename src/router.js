import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
Vue.use(Router)

// это нужно чтоб если зашли например
// на страницу логина и у нас есть токен то сразу перекинуло на главную
const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    // это типо идем дальше
    next()
    return
  }
  // а это идем дальше но на страницу
  next('/')
}
// вариант если должен быть авторизован
const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      // динамический импорт компонтена нужна поддержка в системе сборки
      component: () => import('./views/Home.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue'),
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('./views/Login.vue'),
      beforeEnter: ifNotAuthenticated,
    },
  ]
})
