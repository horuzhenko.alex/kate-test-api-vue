import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token') || '',
    status: '',
  },
  getters: {
    isAuthenticated: state => !!state.token
  },
  mutations: {
    'SET_TOKEN': (state, token) => {
      state.token = token
    },
    'AUTH_LOGOUT': (state) => {
      state.token = ''
      localStorage.removeItem('token')
      this.$router.push('/login')
    },
  },
  actions: {

  }
})
